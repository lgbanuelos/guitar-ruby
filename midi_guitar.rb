class MidiGuitar
  def play(str)
    system "java -jar guitar.jar '#{str}'"
  end
end

if __FILE__ == $0
  MidiGuitar.new.play ARGV[0]
end
